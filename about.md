---
layout: page
title: Sobre
permalink: /about/
---

O Atelier Tech é uma iniciativa para encorajar, motivar e auxiliar desenvolvedores a se auto aperfeiçoar.
Grande parte de nossa inspiração vem do [manifesto software craftsmanship](https://manifesto.softwarecraftsmanship.org/#/pt-br), queremos ser uma comunidade de profissionais e criar parcerias produtivas.