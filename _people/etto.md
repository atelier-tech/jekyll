---
layout: people
name: Ettore Leandro Tognoli
github:
    username: ettoreleandrotognoli
gitlab:
    username: ettoreleandrotognoli
linkedin:
    username: ettore-leandro-tognoli
email:
    address: ettoreleandrotognoli@gmail.com
lattes:
    id: "1563628701928365"
---
