FROM ruby:2.3
RUN mkdir /home/jekyll
WORKDIR /home/jekyll
COPY . .
RUN bundle install
EXPOSE 4000
CMD bundle exec jekyll serve --host=0.0.0.0