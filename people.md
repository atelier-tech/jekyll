---
layout: page
title: Colaboradores
permalink: /people/
---

<ul class="post-list">
    {% for person in site.people %}
    <li class="person">
        <a href="{{ site.baseurl }}{{person.url}}">{{person.name}}</a>
        {% include social-list.html page=person %}
    </li>
    {% endfor %}
</ul>